#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import division
import os

from bean import Bean


class Controller(object):

    def __init__(self, output):
        self.output = output
        self.target_x = 360
        self.target_y = 567
        self.bean_list = []
        self.preload()

    def preload(self):
        self.bean_list = [
            Bean('values', 0, 'default dimension'),
            Bean('values-w320dp', 320, '480x854px 320x569dp 240dpi 1.5x, Android One,LGE LGLS675'),
            Bean('values-w360dp', 360, '1080p or 1440p, 360x640dp nexus5 3.0x, with navigation bar'),
            Bean('values-w384dp', 384, '768x1280px 384x640dp nexus 4 2x'),
            Bean('values-w411dp', 411, '1440x2560px 411x731dp, Nexus 6p 3.5x, Nexus 5X 2.625x'),
            Bean('values-w768dp', 768, '768x1024px 768x1024dp, Tab or pad, 1x'),
        ]

    def generate(self):
        for bean in self.bean_list:
            value_path = "%s/%s" % (self.output, bean.name)
            self.create_dir_if_needed(value_path)
            self.handle(bean, value_path)

    def handle(self, bean, path):
        f = '%s/dimens.xml' % path
        self.delete_if_exist(f)
        self._write_internal(f, self.target_x, bean.width, bean.des)

    def _write_internal(self, path, target, request, des):
        with open(path, 'w') as f:
            line_header = '<?xml version="1.0" encoding="utf-8"?>'
            line_begin = '<resources>'
            line_end = '</resources>'
            f.write(line_header + "\n")
            f.write(line_begin + "\n")
            if des:
                f.write("       <!--%s-->\n" % des)

            for index in range(self.target_y):
                if request > 0:
                    value_f = '%.3f' % (request / target * (index + 1))
                else:
                    value_f = index + 1
                values_str = '      <dimen name="s%s">%sdp</dimen>' % (index + 1, value_f)
                f.write(values_str + "\n")

            f.write(line_end)

    @classmethod
    def create_dir_if_needed(cls, path):
        if not os.path.exists(path):
            os.mkdir(path)

    @classmethod
    def delete_if_exist(cls, path):
        if os.path.exists(path):
            os.remove(path)
