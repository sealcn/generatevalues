#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import os
import sys
from optparse import OptionParser

from controller import Controller
from options import Options


class Cli(object):

    def __init__(self):
        super(Cli, self).__init__()
        self.options = Options()

    def parse_args(self):
        usage = "usage: %prog [options] arg1 arg2"
        parser = OptionParser(usage=usage)
        # parser.add_option('-i', '--input', action='store', type='string', dest='input_path',
        #                   help='Specify the input path that need to be analyse')
        parser.add_option('-o', '--output', action='store', type='string', dest='output_path',
                          help='Specify the output directory')
        (options, args) = parser.parse_args(sys.argv[1:])
        # if not options.input_path:
        #     parser.error("Must specify the input path")
        # else:
        #     if os.path.isdir(options.input_path) or os.path.isfile(options.input_path):
        #         print "the input path: %s" % options.input_path
        #     else:
        #         parser.error("The input path is not correct, can't found")

        if not options.output_path:
            print "Not specify the output path, use default"
            current_dir = os.path.dirname(os.path.realpath(__file__))
            options.output_path = "%s/outputs" % current_dir
            if not os.path.isdir(options.output_path) or not os.path.exists(options.output_path):
                os.mkdir(options.output_path)

        else:
            if not os.path.isdir(options.output_path) and not os.path.isfile(options.output_path):
                parser.error("The output path is not correct, can't found")
        print "the output path: %s" % options.output_path

        # self.options.input_path = options.input_path
        self.options.output_path = options.output_path

    def entry(self):
        self.parse_args()
        controller = Controller(self.options.output_path)
        controller.generate()


if __name__ == '__main__':
    cli = Cli()
    cli.entry()
