#!/usr/bin/env python
# -*- coding: UTF-8 -*-


class Bean(object):

    def __init__(self, name, width, des):
        self.name = name
        self.width = width
        self.des = des

    def __str__(self):
        return "{name=%s, width=%s, des=%s}" % (
            self.name, self.width, self.des)

    def __repr__(self):
        return "{name=%s, width=%s, des=%s}" % (
            self.name, self.width, self.des)
